#include "socket.h"
#include "irc_socket.h"
#include "network_utils.h"
#include "socket_manager.h"
#include "mtalk_socket.h"
#include "options.h"

using namespace Network;
using namespace std;
bool is_running_stats_bot;

char p_seperator = 31;
bool p_find(std::string const buf, std::string key) {
	if(buf.find(">PROTOTEST 0 " + key + "\r\n") == 0) {
		return true;
	} else {
		return false;
	}
}
IrcSocket *g_bot_sock;
void pm(std::string say, Mtalk::Options &o) {
	if(is_running_stats_bot) {
		string tmp = "PRIVMSG " + o.get_status_bot_channel() + " :" + say;
		g_bot_sock->send("PRIVMSG " + o.get_status_bot_channel() + " :" + say +
		  "\r\n");
	}
}
int main (int argc, char *argv[]) {
	//our global options, we pass this around to remember user-generated
	//values.
	Mtalk::Options options;
	if(!options.parse_commandline(argc, argv)) {
		return 1;
	}
	is_running_stats_bot = options.get_is_run_status_bot();
	SocketManager<Socket> sock_manager; //this will track all sockets the server
	MtalkSocket *listen = new MtalkSocket;//uses
	sock_manager.add(listen);
	listen->bind(options.get_listen_port());
	listen->listen(5);

	//add an IRC bot...
	IrcSocket *bot_sock = new IrcSocket;
	bool is_bot_on;
	if(is_running_stats_bot) {
		string bot_ip = "";
		get_host_by_name(options.get_status_bot_network(),bot_ip);
		bot_sock->connect(bot_ip,6667);
		bot_sock->send("NICK " + options.get_status_bot_name() + "\r\nUSER mtalk foo foo :mtalk.nixeagle.org\r\n");
		g_bot_sock = bot_sock;
		is_bot_on = false;
		sock_manager.add(bot_sock);
	} else { is_bot_on = true; }
	while(1) {
		Socket *s = sock_manager.select();
		if(s == listen) { //New connection, accept and add.
			MtalkSocket *ns = listen->accept();
			if(!is_bot_on) { 
				is_bot_on = true;
				bot_sock->send("JOIN " + options.get_status_bot_channel() +
				  "\r\nPRIVMSG " + options.get_status_bot_channel() +
				  " :The mtalk server is up.\r\n");
			}
			sock_manager.add(ns);
			pm("New Connection!",options);
		} else { //Something has happened on an existing socket.
			string buf = "";
			int result = 3;
			MtalkSocket *ms = static_cast<MtalkSocket*>(s);	
			if(s == bot_sock) {
				//leave s be.
				int r = s->recv(buf);
				if(r <= 0) {
					pm("Bot died",options);
					sock_manager.remove(s);
				} else {
					if(buf.find("PING") == 0) {
						buf.replace(0,4,"PONG");
						bot_sock->send(buf);
					} 
				}
				continue;
			} 
			result = ms->recv();
			std::cout << result << std::endl;
			
			if(result <= 0) { //sock was dropped, drop them
				pm("Connection disconnected!",options);
				sock_manager.remove(s);
			} else if (result == 2) {
				
			} else {//send data to everyone else.
				MtalkParser m = ms->get_last_message();
				if(m.get_command() == "MCONNECT" &&
				   m.get_args()[0] == options.get_server_pass() &&
				   m.get_args().size() == 4) {
				   	ms->auth();
					pm("Connection authed",options);
					ms->send("OK\x1F" "0\x1F" "MTALK_PASS_ACCEPTED\x1F" "1\x1F" "0.0.10\x1F" "0.1.0-pre" "\r\n");
				} else if (ms->get_is_authed()) {
					sock_manager.sendall("HIII",listen,s,bot_sock);
				} else {
					pm("Connection disconnected",options);
					sock_manager.remove(s);
				}
			}
		}
	}
}
