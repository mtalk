#include "options.h"
#include "defines.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <string>

namespace po = boost::program_options;
namespace Mtalk {
Options::Options(void) {
}

bool Options::parse_commandline(int argc, char* argv[]) {
	po::options_description generic("Allowed options");
	generic.add_options()
		("help,h", "produce help message")
		("version,v", "prints version string")
	;
	
	//options in both commandline and config file..
	
	po::options_description config("Configuration");
	config.add_options()
		("listen-port,p",
		  po::value<unsigned short>(&listen_port)->default_value(6650),
		  "Set the port for the daemon to listen to.")
		("run-status-bot", 
		  po::value<bool>(&is_run_status_bot)->default_value(false),
		  "Set if status reporter bot is to be enabled. Settings for "
		  "this bot are found in the configuration file")
	;

	po::options_description hidden("Hidden options");
	hidden.add_options()
		("status-bot-name", 
		  po::value<std::string>(&status_bot_name)->
		    default_value("MTalk[daemon]"), "bot name")
		("status-bot-network",
		  po::value<std::string>(&status_bot_network), "network")
		("status-bot-channel",
		  po::value<std::string>(&status_bot_channel), "channel")
	;
	po::options_description config_only("config only options");
	config_only.add_options()
		("server-pass",
		  po::value<std::string>(&server_pass), "server password")
	;
	//Options on the commandline, include all 3 groups above.
	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config).add(hidden);
	//User chosen config file
	po::options_description config_file_options;
	config_file_options.add(config).add(hidden).add(config_only);
	//options from default config file
	po::options_description default_config_file_options;
	default_config_file_options.add(config).add(hidden).add(config_only);

	//what options are to be visible:
	po::options_description visible;
	visible.add(generic).add(config);

	po::variables_map vm;
	po::store(po::parse_command_line(argc,argv, cmdline_options), vm);
	
	//user chosen config..
	std::ifstream ifs("mtalk.cfg");
	po::store(parse_config_file(ifs, config_file_options), vm);

	//default config..
	std::ifstream ifs2("mtalk.default.cfg");
	po::store(parse_config_file(ifs2, config_file_options), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << visible << std::endl;
		return false;
	}
	if (vm.count("version")) {
		std::cout << 
		  "Warning this is alpha software, program outputs may change, " <<
		  std::endl << "and the software may even eat your children!" <<
		  std::endl << "mtalk version " << MTALK_DAEMON_VERSION << std::endl;
		return false;
	}
	if(vm.count("listen-port")) {
		std::cout << "Port: " << vm["listen-port"].as<unsigned short>() << std::endl;
	}
	return true;
}
}
