#ifndef IRC_SOCKET_H
#define IRC_SOCKET
#include "defines.h"
#include "socket.h"
namespace Network {
class IrcSocket : public Socket {
public:
	IrcSocket(int const socket_id);
	IrcSocket(void);
	
	int recv(std::string &data);

private:
	std::string recv_buffer;

};
}

#endif

