#ifndef NETWORK_UTILS_H
#define NETWORK_UTILS_H

#include <string>
#include "defines.h"
//Basic network utils, abstractions

namespace Network {

/**
 * Gets the address of an internet location based on hostname
 * @param name name of host to get, like foo.com
 */
int get_host_by_name(const std::string name,std::string &address);


}

#endif
