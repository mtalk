#include "socket.h"
#include "mtalk_parser.h"
#include <map>
#include <string>
#include <iostream>
namespace Network {
/**
 * 
 */
enum SocketType { NONE, PROTOCOL_HANDLER, CLIENT, PLUGIN };

class MtalkSocket : public Socket {
public:
	MtalkSocket(int const socket_id);
	MtalkSocket(void);

	/**
	 * Parses a line of mtalk protocol.
	 *
	 * This will also place args in a mtalk parser object. Get the
	 * values from there.
	 *
	 * @return Returns an integer. -1 is an error.
	 */
	int recv(void);
	MtalkSocket *accept(void);
	MtalkParser get_last_message(void);

	//note that socket has connected, and has given an ok pass...
	void auth(void);
	bool get_is_authed(void);

private:
	void init(void);
	std::string recv_buffer;
	MtalkParser message;

	bool is_authed;
	std::map<std::string,std::string> info;
};
}
