#include "mtalk_parser.h"
#include "string_utils.h"

#include <iostream>
#include <cassert>
namespace Network {
MtalkParser::MtalkParser(void) : 
  is_recv(false) {
}
bool MtalkParser::parse(std::string msg) {
	//Proper protocol mandates \r\n at the end of messages.
	size_t secondary_endline = msg.find("\r\n");
	size_t mtalk_endline = msg.find("\031\r\n");
	std::string delimiter;
	if(0 == msg.find(">") && (msg.length() - 2) == secondary_endline) {
		msg.erase(secondary_endline,2);
		std::string alt;
		msg.erase(0,1);
		delimiter = " ";
		is_secondary_proto = true;
		//need to search for message contents if any:
		if(msg.find("&&") != std::string::npos) {
			//split...
			alt = msg.substr(0,msg.find("&&") - 1);
			args = Utils::tokenize(alt,delimiter);
			alt = msg.substr(msg.find("&&") + 2);
			args.push_back(alt);
		} else {
			args = Utils::tokenize(msg,delimiter);
		}
	} else if( (msg.length() - 3) == mtalk_endline ) {
		msg.erase(mtalk_endline,3); //Chop off the \031 as well as \r\n
		delimiter = "\x1F";
		is_secondary_proto = false;
		args = Utils::tokenize(msg,delimiter);
	}
	if (true) {
		if(2 > args.size()) { //no valid mtalk command...
			return false;
		}
		//: indicates the message is incoming not outgoing.
		if(args.size() && ':' == args.at(0)[0]) {
			is_recv = true;
			//drop the : at the start...
			args[0].erase(0,1);
		} else {
			is_recv = false;
		}
		return true;
	} else {
		return false;
	}
}
std::string MtalkParser::get_command(void) {
	if(is_recv) {
		return args.at(2);
	} else {
		return args.at(0);
	}
}
std::string MtalkParser::get_networkid(void) {
	if(is_recv) {
		return args.at(3);
	} else {
		return args.at(1);
	}
}
std::string MtalkParser::get_nick(void) {
	if(is_recv) {
		return args.at(0);
	} else {
		return NULL;
	}
}
std::string MtalkParser::get_alternate(void) {
	if(is_recv) {
		return args.at(1);
	} else {
		return NULL;		
	}
}

std::vector<std::string> MtalkParser::get_args(void) {
	std::vector<std::string> args2 = args;
	if(is_recv && args2.size() > 4) {
		args2.erase(args2.begin(),args2.begin() + 4);
		return args2;
	} else if (!is_recv && args2.size() > 2)  {
		args2.erase(args2.begin(),args2.begin() + 2);
		return args2;
	} else {
		args2.clear();
		return args2;
	}
}
}
