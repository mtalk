#include <network_utils.h>
#ifdef WIN32
	#include <winsock.h>

	//to make linker errors go away:
	#pragma comment(lib, "winmm.lib")
	#pragma comment(lib, "wsock32.lib")
	#pragma comment(lib, "advapi32.lib")
#else
	#include <netdb.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#endif
int Network::get_host_by_name(const std::string name, std::string &address) {
	struct hostent *host;
	host = gethostbyname(name.c_str());

	if(NULL == host) { //we have a problem
		return 1;
	}
	//get the address we care about.
	address = inet_ntoa(*((struct in_addr *)host->h_addr));
	return 0;

}
