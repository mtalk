#include "mtalk_socket.h"
#ifdef WIN32
	#include <winsock.h>
	typedef char raw_data;
	typedef int socklen_t;
#else
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <unistd.h>
	#include <netinet/in.h>
	#include <fcntl.h>
	#include <string.h>
	typedef void raw_data;
#endif
#include <errno.h>
#include <cassert>
namespace Network {
MtalkSocket::MtalkSocket(int const socket_id) : Socket(socket_id) {
	recv_buffer.erase();
	is_authed = false;
}
MtalkSocket::MtalkSocket(void) : Socket() {
	recv_buffer.erase();
	is_authed = false;
}
using namespace std;
using namespace std;
using namespace std;
int MtalkSocket::recv() {
	//lets check the recv buffer, if we can return something
	//from there, do so, otherwise try looking to recv something
	int newline_loc = recv_buffer.find("\r\n");
	if (std::string::npos == newline_loc) { //try looking at the sock
		std::string buffer = "";
		int size = Socket::recv(buffer);
		if(0 >= size) { //we have an error with recv.
			#ifndef WIN32
			herror("?"); //can't use this on windows...
			#endif
			return -1; //set herror to something in the socket var.
		} 
		recv_buffer += buffer;

		//see if recv buffer has a new line now.
		newline_loc = recv_buffer.find("\r\n");
	}
	//now if we actually have something...
	if(std::string::npos != newline_loc) {
	//now we can parse out the newline and return just that:
		std::string data = recv_buffer.substr(0,newline_loc + 2);
		recv_buffer.erase(0,newline_loc + 2); //remeber the \r\n count!
		if(!message.parse(data)) {
			return 1;//can't parse...
		} else {
			return 3;//good
		}
	} else { //nothing
		return 2;//nothing ending in /r/n
	}
}
MtalkParser MtalkSocket::get_last_message(void) {
	return message;
}

MtalkSocket *MtalkSocket::accept(void) {
	int newSocket = ::accept(get_socketid(), NULL, 0);
	if(0 <= newSocket) {
		return new MtalkSocket(newSocket); 
	} else {
		assert(0);
	}
}

void MtalkSocket::auth(void) {
	is_authed = true;
}

void MtalkSocket::init(void) {
	is_authed = false;
}
bool MtalkSocket::get_is_authed(void) {
	return is_authed;
}
}
