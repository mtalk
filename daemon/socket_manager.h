#ifndef SOCKET_MANAGER_H
#define SOCKET_MANAGER_H

#include <iostream>
#include <string>
#include <cassert>
#include "socket.h"
#include <vector>
#include <string.h>

#ifdef WIN32
#include <winsock.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

namespace Network {
typedef Socket socket_type;
template <class T>
class SocketManager {
        std::vector<T*> socks;
	fd_set master;
	fd_set readfs;
	int fdmax;

private:
	 T *current_sock;
public:
	SocketManager(void) : current_sock(NULL) {
		
	}
	void add(T *sock) {
		socks.push_back(sock);
		if(sock->get_socketid() > fdmax) {
			fdmax = sock->get_socketid();
		}
	}
	void remove(T *sock) {
		typename std::vector<T *>::iterator iter;
		for(iter = socks.begin();
		    iter != socks.end();
		    iter++) {
			T *s = *iter;
			if(s == sock) { //same sockets
				s->close();
				delete *iter;
				socks.erase(iter);
				return;
			}
		}
		//if we get here, something went wrong, we tried to delete something
		//that is not in the vector.
		assert(0);
	}
	int listen_prepare(void) {
		FD_ZERO(&master);
		int conn_counter = 0;
		for(typename std::vector<T*>::iterator iter = socks.begin();
		    iter != socks.end();
		    iter++, conn_counter++) {
			T *s = *iter;
			if(s->get_socketid() > fdmax) {
				fdmax = s->get_socketid();
			}
			FD_SET(s->get_socketid(),&master);
		}
		return conn_counter;
	}
	bool isset(T *sock) {
		if(FD_ISSET(sock->get_socketid(),&master)) { 
			return true;
		} else {
			return false;
		}
	}
	T *select(void) {
		listen_prepare();
		if(::select(fdmax+1,&master,NULL,NULL,NULL) == -1) {
			assert(0);
		}
	//	if(current_sock == NULL) {
			current_sock = *socks.begin();
	//	}
		bool check = false;
		for(typename std::vector<T*>::iterator iter = socks.begin();
		    iter != socks.end();
		    iter++ ) {
		    // see if we have checked this already this round:
			if(!check) {
				if( current_sock == *iter) {
					check = true;
				} else {
					continue;
				}
			}
			T *s = *iter;
			current_sock = s;
			if(FD_ISSET(s->get_socketid(),&master)) {
				return s;
			}
		}
		current_sock = NULL;
	}
	int sendall(std::string data,
	            T *listener = NULL,
		    T *sender = NULL,
		    T *extra = NULL) {
		int counter = 0;
		for(typename std::vector<T*>::iterator iter = socks.begin();
		    iter != socks.end();
		    iter++ ) {
			T *s = *iter;
			if(!isset(s)) {
				if(s != listener && s != sender && s != extra) {
					s->send(data);
					counter++;
				}
			}
		}
		return counter;
	}
		    
private: 
};
}
#endif

