//mtalk_parser.h - mtalk's protocol parser
//Mtalk's protocol parser.
//
//Copyright James Spahlinger 2008

#ifndef MTALK_PARSER_H
#define MTALK_PARSER_H
#include "string_utils.h"

#include <string>
#include <vector>
namespace Network {
class MtalkParser {
public:
	MtalkParser(void);
	
	//parse given mtalk string. If its invalid, we return false, else
	//you will get true in response.
	bool parse(std::string);
	
	std::string get_command(void);
	std::string get_networkid(void);
	std::string get_nick(void); //nick is the screen name, the name that is
	                           //used to comunicate with someone.
	std::string get_alternate(void); //additional info about the user.

	std::vector<std::string> get_args(void);
private:
	std::vector<std::string> args;
	bool is_recv; // are we getting a request to send or a requset to
	              // recieve a message.
	bool is_secondary_proto;
};
}



#endif
