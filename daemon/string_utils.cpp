#include "string_utils.h"

namespace Utils {

/* This function derived from 
 * http://www.oopweb.com/CPP/Documents/CPPHOWTO/Volume/C++Programming-HOWTO-7.html#ss7.3
 * License at:
 * http://www.oopweb.com/CPP/Documents/CPPHOWTO/Volume/C++Programming-HOWTO-22.html
 */
std::vector<std::string> tokenize(const std::string &str,
                                  const std::string delimiter) {
	std::vector<std::string> args;
	std::string::size_type last_pos = str.find_first_not_of(delimiter, 0);
	std::string::size_type pos     = str.find_first_of(delimiter, last_pos);

	while(std::string::npos != pos || std::string::npos != last_pos) {
		args.push_back(str.substr(last_pos, pos - last_pos));
		//skip delimeter
		last_pos = str.find_first_not_of(delimiter, pos);
		//find next non-delimeter.
		pos = str.find_first_of(delimiter, last_pos);
	}
	return args;
}
}

