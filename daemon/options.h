//This software is GPLv3 software
//The point of this object is to contain global information.
//There should only be one instance of this object ever.

#ifndef OPTIONS_H
#define OPTIONS_H

#include <string>
namespace Mtalk {
/**
 * This is our global configuration options. Anything that must be
 * set at runtime is set here. There should never be more then one copy
 * of this object in existance at one time.
 */
class Options {
public:
	/**Constructor - Should initialize all values to sensible defaults:
	 * 
	 * Configuration order can be overridden like this:
	 * 1. User options at command line override all considerations
	 * 2. User options in user's config file override all but 1)
	 * 3. Default config file override all but 1) and 2)
	 * 4. Items are set to default values in the constructor.
	 */
	Options(void); 
	
	/**Take commandline args, and parse them.
	 *
	 * @return boolean value. false means we had some problem parsing.
	 * This may or may not change in the future.
	 */
	bool parse_commandline(int argc, char* argv[]);
	/**Get the port to listen on for daemon binding */
	unsigned short get_listen_port(void) { return listen_port; }
	/**Set the port to listen on for daemon binding */
	void set_listen_port(unsigned short port) { listen_port = port; }
	
	std::string get_server_pass(void) { return server_pass; }
	bool get_is_run_status_bot(void) {return is_run_status_bot;}
	std::string get_status_bot_name(void) {return status_bot_name;}
	std::string get_status_bot_network(void) { return status_bot_network;}
	std::string get_status_bot_channel(void) { return status_bot_channel;}
protected:
	Options(const Options&);
	Options &operator= (const Options&);

private:
	//The port we listen to for incoming connections.
	unsigned short listen_port;
	///Password that users set for auth'ing to the server with plugins.
	std::string server_pass;
	///Do we or do we not run the bot, default to false.
	bool is_run_status_bot;
	///Name of attached statusbot.
	std::string status_bot_name;
	std::string status_bot_network;
	std::string status_bot_channel;
};
}

#endif
