#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>
#include <vector>
namespace Utils {
	//Tokenize strings, think split in perl, and explode in php.
	std::vector<std::string> tokenize(const std::string &str,
	                                  const std::string delimeter);
}

#endif
