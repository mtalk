#ifndef SOCKET_H
#define SOCKET_H

#include <string>

#ifdef WIN32
#include <winsock.h>
#else
#include <netinet/in.h>
#include <socket.h>
#include <arpa/inet.h>
#endif
namespace Network {
class Socket {
public:
	//This class is meant to be inherited.
	Socket(int const socket_id);
	Socket(void);
public:
	~Socket(void);
	
	//bind to a port. Returns any error messages it may get.
	int bind(unsigned short port);
	Socket *accept(void);
	int connect(const std::string url,const unsigned short port);
	//we are goign to make a promise that all given data will be sent.
	int send(const std::string data);

	int recv(std::string &data); //data will be changed.
	int listen(const int backlog);
	int close(void);
	//does not fully close the socket, only removes one or both
	//ways of communicating through it.
	int shutdown(const int how);
	
	//This is different on windows of course :<
	void set_nonblocking(void);

	//getter and setter functions:
	int get_socketid(void);
	unsigned short get_local_port(void);
	unsigned short get_remote_port(void);
	std::string get_url(void);

	unsigned short get_peer_port(void) { return ntohs(peeraddr.sin_port); }
	std::string get_peer_address(void) { return inet_ntoa(peeraddr.sin_addr); }
private:
	void initialize_winsock(void);
	sockaddr_in create_address(unsigned short port,
	                           std::string addr = "");
	Socket(const Socket &sock);
	void operator=(const Socket &sock);


private:
	int socket_id; //socket file descriptor.
	unsigned short local_port; //port on local computer
	unsigned short remote_port; //port on remote computer.
	int max_buffer_size;
	struct sockaddr_in peeraddr;
};
}

#endif

