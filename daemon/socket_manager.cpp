#include "socket_manager.h"

namespace Network {

void SocketManager::add(socket_type *sock) {
	socks.push_back(sock);
	if(sock->get_socketid() > fdmax) {
		fdmax = sock->get_socketid();
	}
}
socket_type *SocketManager::select(void) {
	listen_prepare();
	if(::select(fdmax+1,&master,NULL,NULL,NULL) == -1) {
		assert(0);
	}
//	if(current_sock == NULL) {
		current_sock = *socks.begin();
//	}
	bool check = false;
	for(sockvec::iterator iter = socks.begin();
	    iter != socks.end();
	    iter++ ) {
	    // see if we have checked this already this round:
		if(!check) {
			if( current_sock == *iter) {
				check = true;
			} else {
				continue;
			}
		}
		socket_type *s = *iter;
		current_sock = s;
		if(FD_ISSET(s->get_socketid(),&master)) {
			return s;
		}
	}
	current_sock = NULL;
}
SocketManager::SocketManager(void) : current_sock(NULL) {}
void SocketManager::remove(socket_type *sock) {
	for(sockvec::iterator iter = socks.begin();
	    iter != socks.end();
	    iter++) {
		socket_type *s = *iter;
		if(s == sock) { //same sockets
			s->close();
			delete *iter;
			socks.erase(iter);
			return;
		}
	}
	//if we get here, something went wrong, we tried to delete something
	//that is not in the vector.
	assert(0);
}
bool SocketManager::isset(socket_type *sock) {
	if(FD_ISSET(sock->get_socketid(),&master)) { 
		return true;
	} else {
		std::cout << "false: " << sock->get_socketid() << std::endl;
		return false;
	}
}
int SocketManager::listen_prepare(void) {
	FD_ZERO(&master);
	int conn_counter = 0;
	for(sockvec::iterator iter = socks.begin();
	    iter != socks.end();
	    iter++, conn_counter++) {
		socket_type *s = *iter;
		if(s->get_socketid() > fdmax) {
			fdmax = s->get_socketid();
		}
		FD_SET(s->get_socketid(),&master);
	}
	return conn_counter;
}
int SocketManager::sendall(std::string data,
                           socket_type *listener,
                           socket_type *sender) {
	int counter = 0;
	for(sockvec::iterator iter = socks.begin();
	    iter != socks.end();
	    iter++ ) {
		Socket *s = *iter;
		if(!isset(s)) {
			if(s != listener && s != sender) {
				s->send(data);
				counter++;
			}
		}
	}
	return counter;

}
}
