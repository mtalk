#include <iostream>
#include <string>

#include "daemon/socket.h"
#include "daemon/network_utils.h"

using namespace Network;
using namespace std;
int main() {
//Lets see how hard it is to connect to devnode as a test of sockets...
	cout << "Starting test of socket client functions..." << endl;
	Socket *sock = new Socket;
	
	//we have a socket, lets try to resolve devnode.
	string devnode_ip = "";
	//using ares, because its pings make sense and are easy to 
	//duplicate using this primitive test.
	get_host_by_name("dionysus.devnode.org",devnode_ip);

	cout << "IP of irc.devnode.org is: " << devnode_ip << endl;
	
	//now attempt to connect to devnode:
	sock->connect(devnode_ip,6667);
	
	sock->send("NICK testtest\r\nUSER foo foo foo :foo\r\nJOIN #nixeagle\r\n");

	string data = "";
	string olddata = "";
	int loc = string::npos;
	while(1){
		sock->recv(data);
		data = olddata + data;
		while(1) {
		loc = data.find("\r\n");
		if(loc != string::npos) {
			string line = data.substr(0,loc);
			data = data.substr(loc+2);
			cout << line << endl;
		if(line.find("asdf") != string::npos) {
			sock->send("PRIVMSG #nixeagle :you called?\r\n");
		}
		} else { olddata = data; break;}
		olddata = "";
		}
	}
	delete sock;

}
