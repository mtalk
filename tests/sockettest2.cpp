#include <iostream>
#include <string>

#include "daemon/irc_socket.h"
#include "daemon/network_utils.h"
using namespace Network;
using namespace std;
int main() {
	IrcSocket *sock = new IrcSocket;
	
	//we have a socket, lets try to resolve devnode.
	string devnode_ip = "";
	get_host_by_name("dionysus.devnode.org",devnode_ip);

	//now attempt to connect to devnode:
	sock->connect(devnode_ip,6667);
	
	sock->send("NICK bar\r\nUSER foo foo foo :foo\r\nJOIN #mtalk\r\n");
	string data = "";
	int loc = string::npos;
	while(1){
		sock->recv(data);
		if(data.find("asdf") != string::npos) {
			sock->send("PRIVMSG #mtalk :jkl;\r\n");
		}
		if(data.find("serveraddress") != string::npos) {
			sock->send("PRIVMSG #mtalk :" + sock->get_peer_address() + "\r\n");
		}
		cout << data << endl;
	}
}
