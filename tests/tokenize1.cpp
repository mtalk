//Copyright James Spahlinger 2008
//Just a few tests of string_utils
#include "string_utils.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Utils;

const string correct_output = "[HI] [HOW] [ARE] [YOU] ";
int test_number = 0; //yes I know global... shoot me, its a test program

bool test_tokenize(string delim) {
	vector<string> args;
	string str = "HI" + delim + "HOW" + delim +"ARE" + delim + "YOU";
	args = tokenize(str,delim);
	string output = "";
	for(vector<string>::iterator iter = args.begin();
	    iter != args.end();
	    iter++) {
		output += "[" + *iter + "] ";
	}
	test_number++;

	if(output != correct_output) {
		cerr << "Test " << test_number 
		     << " FAIL, incorrect parsing of: " << delim
		     << endl << "Output was: " << output << endl
		     << "Correct output is: " << correct_output << endl;
		return false;
	} else {
		cout << "Test " << test_number << " PASS" << endl;
	}
	return true;

}
int main () {
	cout << "This is a test of mtalk's tokenize function." << endl
	     << "Several strings are sent to the function and returned." 
	     << endl << "Correct output for all strings is: "
	     << correct_output << endl;
	if(!test_tokenize(" ")) { return 1;} //spaces, standard.
	if(!test_tokenize("\x1F")) { return 1;}//mtalk protocol
	if(!test_tokenize("  ")) { return 1;}//multiple chars as delimiter.
	cout << "Tests passed" << endl;
	return 0;



}
