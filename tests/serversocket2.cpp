#include <iostream>
#include <string>
#include <cassert>
#include "socket.h"
#include <string.h>

#ifdef WIN32
#include <winsock.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
using namespace Network;
using namespace std;
int main () {
	//some nasty stuff to do select
	fd_set master, read_fs;
	int fdmax;
	int newsock;
	FD_ZERO(&master);
	FD_ZERO(&read_fs);

	Socket listen;

	cout << "Trying to bind to a socket:" << endl;
	listen.bind(6650);
	listen.listen(5);
//	listen.set_nonblocking();

	FD_SET(listen.get_socketid(), &master);
	fdmax = listen.get_socketid();
	while(1) {
		//update read_fds
		read_fs = master;
		if(select(fdmax+1,&read_fs,NULL,NULL,NULL) == -1) {
			perror("listen");
			assert(0);
		}
		//run through existing connectinos...
		for(int i = 0; i<= fdmax;i++) {
			if(FD_ISSET(i,&read_fs)) {
			cout << "ISSET " << i << endl;
				if(i == listen.get_socketid()) {
					Socket *ns = listen.accept();
				//	Socket ns(newsock);
				//	ns.set_nonblocking();
					FD_SET(ns->get_socketid(),&master);
					if(ns->get_socketid() > fdmax) {
						fdmax = ns->get_socketid();
					}
					cout << "new socket: " << ns->get_socketid() << endl;
					cout << "listen sock: " << listen.get_socketid() << endl;
				} else { //old sock
					string buf = "";
					Socket *oldsock = new Socket(i);
					int result = oldsock->recv(buf);
					if(result <= 0) { //drop..
						cout << "connection closed" <<endl;
						oldsock->close();
						delete oldsock;
						FD_CLR(i,&master);
					} else { //some data
						for(int j = 0;j<= fdmax; j++) {
						if( FD_ISSET(j,&master)) {
							if (j != listen.get_socketid() && j != i) {
								Socket *ns = new Socket(j);
								ns->send(buf);
							}
						}
						}
					}
				}
			}
		}
	}
	return 0;
}
