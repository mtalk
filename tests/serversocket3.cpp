#include "socket.h"
#include "socket_manager.h"
using namespace Network;
using namespace std;
const unsigned int PORT = 6650;
int main () {
	SocketManager<Socket> sock_manager; //this will track all sockets the server
	Socket *listen = new Socket;//uses
	sock_manager.add(listen);
	listen->bind(PORT);
	listen->listen(5);
	while(1) {
		Socket *s = sock_manager.select(); //get next sock in line.
		if(s == listen) { //New connection, accept and add.
			Socket *ns = listen->accept();
			cout << "new connection, id is: " <<
			ns->get_socketid() << endl;
			sock_manager.add(ns);
		} else { //Something has happened on an existing socket.
			string buf = "";
			
			int result = s->recv(buf);
			if(result <= 0) { //sock was dropped, drop them
				sock_manager.remove(s);
			} else { //send data to everyone else.
				sock_manager.sendall(buf,listen,s);
			}
		}
	}
}
