#include "mtalk_parser.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;
using namespace Network;

string mtalk1 = "S\x1F" "NetworkID\x1F" "Arg\031\r\n";
string mtalk2 = ":nick\x1F" "alt\x1F" "S\x1F" "NetworkID\x1F" "Arg\031\r\n";
string mtalk3 = ">S NetworkID Arg\r\n";
string mtalk4 = ">S NetworkID &&Arg\r\n";
string mtalk5 = ">:nick alt S NetworkID Arg\r\n";
//Tests that are meant to fail.
string mtalk6 = "a\r\n";
int testnum = 0;
MtalkParser parser;

void error(string details = "") {
	cerr << "TEST " << testnum << " FAILED" << endl
	     << "Details: " << details << endl;
}
bool parsertest(string test,bool is_recv) {
	testnum++;
	if(parser.parse(test)) {
		//ok parse... try outputing the results
		if("S" != parser.get_command()) {
			error("Get command does not match");
		} else if ("NetworkID" != parser.get_networkid()) {
			error("Get networkid does not match");
		} else if ("Arg" != parser.get_args().at(0)) {
			error("Get args().at(0) does not match");
		} else if (is_recv && "nick" != parser.get_nick()) {
			error("Get nick does not match");
		} else if (is_recv && "alt" != parser.get_alternate()) {
			error("Get alt does not match");
		} else {
			cout << "TEST " << testnum << " PASSED" << endl;
			return true;
		}
		return false;
	} else {
		error("failed to parse the test...");
		return false;
	}
}
int main() {
	cout << "This is a test of the mtalk parser" << endl;
	if(!parsertest(mtalk1,false)) {
		return 1;
	}
	if(!parsertest(mtalk2,true)) {
		return 2;
	}
	if(!parsertest(mtalk3,false)) {
		return 3;
	}
	if(!parsertest(mtalk4,false)) {
		return 4;
	}
	if(!parsertest(mtalk5,true)) {
		return 5;
	}

	//Tests meant to fail
	if(parsertest(mtalk6,false)) {
		return 6;	
	}
	cout << "ALL TESTS PASSED" << endl;
	return 0;
}
