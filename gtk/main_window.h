/***************************************************************************
 *   Copyright (C) 2008 by James Spahlinger   *
 *   eagle@nixeagle.org   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H


#include <gtkmm.h>
#include <sideview.h>
#include <channelview.h>
#include <bufsocket.h>
#include <fcntl.h>

class MTalk : public Gtk::Window {
public:
	MTalk();
	virtual ~MTalk();

protected:
  //Signal handlers:
	//virtual void text(Glib::ustring data);
	virtual bool input_key_released(GdkEventKey* event);
	
  //Member widgets:
	Gtk::HPaned top_horizontal_box;
	Gtk::VPaned top_virtical_box;
	Gtk::VBox text_split_box; ///Place channels and input box here.
	Gtk::ScrolledWindow scroll_window;///text view goes here.
	Glib::RefPtr<Gtk::TextBuffer> text_buffer;///Incoming items.
	Gtk::TextView text_view;///Reads only from the text_buffer for now
	
	Gtk::Entry user_input;///Inputs text into a network connection.

	Gui::SideView sideview;
	Gui::ChannelView chanview1;
	Gui::ChannelView chanview2;
	//testing only
	Net::BufSocket sock;
	Glib::RefPtr<Glib::IOChannel> iochannel;
	bool Test(Glib::IOCondition io_condition);
	bool Test1();
};
#endif
