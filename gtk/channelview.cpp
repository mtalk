//
// C++ Implementation: channelview
//
// Description: 
//
//
// Author: James Spahlinger <eagle@nixeagle.org>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "channelview.h"

namespace Gui {

ChannelView::ChannelView() {
	//put our text view in a scrollbox.
	scroll_window.add(text_view);
	scroll_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	//Set various text view options. We do not load a buffer.
	text_view.set_wrap_mode(Gtk::WRAP_WORD);
	text_view.set_editable(false);

	//show the elements:
	text_view.show();
	scroll_window.show();
	user_input.show();

	//pack the elements: We want to make the view take up all availible space
	//with the input taking up the remaining space.
	pack_start(scroll_window,Gtk::PACK_EXPAND_WIDGET);
	pack_start(user_input,Gtk::PACK_SHRINK,0);
	
}


ChannelView::~ChannelView()
{
}

void ChannelView::set_buffer(const Glib::RefPtr<Gtk::TextBuffer>& buffer) {
	text_view.set_buffer(buffer);
}
void ChannelView::adjust_view(int value) {
	if (0 == value) { //adjust the view to the bottom
		Gtk::Adjustment *adjust = scroll_window.get_vadjustment();
		double upper = adjust->get_upper();
		adjust->set_value((upper - adjust->get_page_size()));
	}
}
}
