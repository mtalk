//
// C++ Interface: channelview
//
// Description:
//
//
// Author: James Spahlinger <eagle@nixeagle.org>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GUICHANNELVIEW_H
#define GUICHANNELVIEW_H
#include <gtkmm.h>

namespace Gui {

/**
	@author James Spahlinger <eagle@nixeagle.org>
*/
class ChannelView : public Gtk::VBox {
public:
	ChannelView();

	~ChannelView();

	void set_buffer(const Glib::RefPtr<Gtk::TextBuffer>& buffer);
	void adjust_view(int value);
protected:
	Gtk::ScrolledWindow scroll_window;///text view goes here.
	/**
	 * We store the text view, but do not store the buffer in this object.
	 * The reason for this is that views can show multiple buffers, depending
	 * on what channel the user wishes to display in any particular view.
	 */
	Gtk::TextView text_view;
public:
	/**
	 * Each channel view is going to have to have its own input box. Not doing
	 * so would probably be confusing to users.
	 * @todo Possibly make this configurable, either one text entry for the
	 * whole application, or one per view. Default to one per view.
	 */
	Gtk::Entry user_input;

	//non-gui related items, these may eventually be associated with the 
	//text *buffers*
	Glib::ustring channel;
	Glib::ustring domain;

};

}

#endif
