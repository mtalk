/***************************************************************************
 *   Copyright (C) 2008 by James Spahlinger   *
 *   eagle@nixeagle.org   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


// newer (non customized) versions of this file go to main_window.cc_new

// This file is for your program, I won't touch it again!

#include "config.h"
#include "main_window.h"
#include <iostream>
#include <errno.h>

MTalk::MTalk()
   // creates a new button with label "Hello World".
{
  // Sets the border width of the window.
	set_border_width(10);
	set_default_size(500,400); //open to something sensible...
	set_position(Gtk::WIN_POS_CENTER); //put app at the center...
	add(top_horizontal_box);
	
	//pack the sideview, which will contain network info, etc.
	top_horizontal_box.add1(sideview);
	sideview.show();
	
	
	// we have to create a buffer
	// this will need to occur for every context/conversation/channel
	text_buffer = Gtk::TextBuffer::create(); 
	
	chanview1.set_buffer(text_buffer);
	chanview1.show();
	chanview2.set_buffer(text_buffer);
	chanview2.show();
	top_virtical_box.add1(chanview1);
	top_virtical_box.add2(chanview2);
	top_horizontal_box.add2(top_virtical_box);
	text_split_box.show();
	top_virtical_box.show();
	top_horizontal_box.show();


	chanview1.user_input.signal_key_release_event().connect(sigc::mem_fun(*this,&MTalk::input_key_released));
	chanview2.user_input.signal_key_release_event().connect(sigc::mem_fun(*this,&MTalk::input_key_released));
}
bool MTalk::Test(Glib::IOCondition io_condition) {
	Gtk::TextIter end = text_buffer->end();
	Glib::ustring str = "";
	sock.recv(str);
	if(str.find("PING :ares.devnode.org")) {
		//sock.Send("PONG :ares.devnode.org\r\n");
	}
	if(str != "\0") {
		text_buffer->insert(end, str /*+ "\n"*/);
	} else {
		std::cerr << "trying to print empty string\n" << std::endl;
	}
	Gtk::Adjustment *adjust = scroll_window.get_vadjustment();
	double upper = adjust->get_upper();
	adjust->set_value((upper - adjust->get_page_size()));
	
	return true;

}
bool MTalk::Test1() {
	Gtk::TextIter end = text_buffer->end();
	Glib::ustring str = "";
	sock.recv_all(str);
	if(str.find("PING :ares.devnode.org")) {
		//sock.Send("PONG :ares.devnode.org\r\n");
	}
	if(!str.empty()) {
		text_buffer->insert(end, str /*+ "\n"*/);
	} else {
		std::cerr << "trying to print empty string\n" << std::endl;
		return false; // still should not be going here..
	}
	chanview1.adjust_view(0);
	chanview2.adjust_view(0);
	
	return true;
}
MTalk::~MTalk() {}

bool MTalk::input_key_released(GdkEventKey* event) {
	//If enter is pressed, the text needs forwarded to the buffer, as well as
	//sent down a network.
	if(36 == event->hardware_keycode) {
		//finding the end of the buffer, we insert next line there:
		Gtk::TextIter end;
		end = text_buffer->end();
		text_buffer->insert(end,chanview1.user_input.get_text() + "\n");
		end = text_buffer->end();
		Glib::ustring test = user_input.get_text();
		if (0 == chanview1.user_input.get_text().find("S ")) {
			Glib::ustring nick = chanview1.user_input.get_text().substr(2,user_input.get_text().find(" ",2));
			sock.connect_by_dns("ares.devnode.org",6667);
			while(!sock.is_connected()) {}
			sock.signal_detected.connect(sigc::mem_fun(*this, &MTalk::Test1));
			//Glib::signal_io().connect(sigc::mem_fun(*this, &MTalk::Test), sock.get_sock(), Glib::IO_IN);
			sock.send("NICK " + nick + "\r\nUSER mtalk mtalk mtalk mtalk\r\nJOIN #mtalk\r\n");
		} else if (0 == chanview1.user_input.get_text().find("MONONET ")) {
			Glib::ustring nick = chanview1.user_input.get_text().substr(2,user_input.get_text().find(" ",8));
			sock.connect_by_dns("mono.ath.cx",56789);
			while(!sock.is_connected()) {}
			sock.signal_detected.connect(sigc::mem_fun(*this, &MTalk::Test1));
			sock.send("USER " + nick );
		}else if(user_input.get_text() == "START") {
			Glib::signal_io().connect(sigc::mem_fun(*this, &MTalk::Test), sock.get_sock(), Glib::IO_IN);
		} else if (user_input.get_text() == "C") {
			sock.send("NICK MTalk\r\nUSER mtalk mtalk mtalk mtalk\r\nJOIN #botters\r\nJOIN #programming\r\n");
		} else if (user_input.get_text() == "P") {
			sock.send("PONG :ares.devnode.org");
		} else if (user_input.get_text() == "TEST") {
			if(sock.is_connected()) { 
				text_buffer->insert(end,"CONNECTED\n");
			} else {
				text_buffer->insert(end,"NOTCONNECTED\n");
			}
		} else {
			test += "\r\n";
			sock.send(test);
		}
			chanview1.adjust_view(0);
			chanview2.adjust_view(0);
	}
}
