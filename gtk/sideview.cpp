//
// C++ Implementation: sideview
//
// Description:
//
//
// Author: James Spahlinger <eagle@nixeagle.org>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "sideview.h"

namespace Gui {

SideView::SideView() {
	//make our treeview for the networks scroll.
	net_scroll_window.add(net_tree_view);
	net_scroll_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	
	//pack the various items.
	pack_start(net_scroll_window);
	
	//we are done setting up everything, we need to show it.
	net_tree_view.show();
	net_scroll_window.show();
}


SideView::~SideView() {
}


}
