//
// C++ Interface: sideview
//
// Description: Contains the channel and network view.
//
//
// Author: James Spahlinger <eagle@nixeagle.org>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GUISIDEVIEW_H
#define GUISIDEVIEW_H

#include <gtkmm.h>

namespace Gui {

/**
	@author James Spahlinger <eagle@nixeagle.org>
*/
class SideView  : public Gtk::VBox {
public:
	SideView();
	~SideView();

	//define columns: thus class inside of a class
	class ModelColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		ModelColumns() {
			add(m_col_id);
			add(m_col_name);
		}

		Gtk::TreeModelColumn<int> m_col_id;
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};

protected:
	ModelColumns tree_view_columns;
	Gtk::ScrolledWindow net_scroll_window;
	Gtk::TreeView net_tree_view;
	Glib::RefPtr<Gtk::TreeStore> ref_tree_model;


};

}

#endif
